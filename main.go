package main

import (
	"bytes"
	"os"
	"os/exec"
	"strconv"

	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"

	"github.com/mattn/go-gtk/gdk"
	"github.com/mattn/go-gtk/glib"
	"github.com/mattn/go-gtk/gtk"
	homedir "github.com/mitchellh/go-homedir"
	"github.com/tink-ab/tempfile"
)

const gyazoFileName = "~/.gyazo.id"

var gyazoid = "snima4"

//////////////////////////////////////////////////////
var defaultURL = "https://url4e.com/gyazo/upload.php"
var defaultQuality = 85

/////////////////////////////////////////////////////
var statusbarContextID uint
var uploadURLInput *gtk.Entry
var qualityNumberInput *gtk.Entry
var statusbar *gtk.Statusbar

type screenShotMetadata struct {
	App   string `json:"app,omitempty"`
	Title string `json:"title,omitempty"`
	URL   string `json:"url,omitempty"`
	Note  string `json:"note,omitempty"`
}

var (
	// Trace - a trace log
	Trace *log.Logger
	// Info - an info log
	Info *log.Logger
	// Warning - a warning log
	Warning *log.Logger
	// Error - an error log
	Error *log.Logger
)

func initLoggers(
	traceHandle io.Writer,
	infoHandle io.Writer,
	warningHandle io.Writer,
	errorHandle io.Writer) {

	Trace = log.New(traceHandle, "TRACE: ", log.Ldate|log.Ltime|log.Lshortfile)
	Info = log.New(infoHandle, "", log.Ldate|log.Ltime)
	Warning = log.New(warningHandle, "WARNING: ", log.Ldate|log.Ltime|log.Lshortfile)
	Error = log.New(errorHandle, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
}

func minInt(x, y int) (r int) {
	if x < y {
		return x
	}
	return y
}
func maxInt(x, y int) (r int) {
	if x > y {
		return x
	}
	return y
}

func captureScreenshot(quality int) (filename string) {
	tmpFile, err := tempfile.TempFile("", "image_upload_", ".jpg")
	if err != nil {
		Error.Println("Can not create temporary image file")
		panic(err)
	}
	fname := tmpFile.Name()
	Info.Println("File: ", tmpFile.Name())
	setStatus("Select capture area")
	importCommand := exec.Command("import", "-quality", strconv.Itoa(quality), fname)
	_, iErr := importCommand.CombinedOutput()
	if iErr != nil {
		Error.Println("While running importCommand:", importCommand)
		panic(iErr)
	}
	return fname
}

func setStatus(newStatus string) {
	statusbar.Pop(statusbarContextID)
	statusbar.Push(statusbarContextID, newStatus)
	gtk.MainIterationDo(true)
}

func postFile(filename string, metadata screenShotMetadata, targetURL string) (string, error) {
	bodyBuf := &bytes.Buffer{}
	bodyWriter := multipart.NewWriter(bodyBuf)
	fileWriter, err := bodyWriter.CreateFormFile("imagedata", filename)
	if err != nil {
		Error.Println("error writing to buffer")
		return "", err
	}
	fh, err := os.Open(filename)
	if err != nil {
		Error.Println("error opening file")
		return "", err
	}
	defer fh.Close()
	//iocopy
	_, err = io.Copy(fileWriter, fh)
	if err != nil {
		return "", err
	}
	contentType := bodyWriter.FormDataContentType()
	bodyWriter.WriteField("id", gyazoid)
	smetadata, err := json.Marshal(metadata)
	if err != nil {
		return "", err
	}
	bodyWriter.WriteField("metadata", string(smetadata))
	bodyWriter.Close()
	resp, err := http.Post(targetURL, contentType, bodyBuf)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	newgyazoid := resp.Header.Get("X-Gyazo-Id")
	if newgyazoid != "" && newgyazoid != gyazoid {
		storeNewGyazoID(newgyazoid)
	}
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	responseURL := string(respBody)
	return responseURL, nil
}

func storeNewGyazoID(newgyazoid string) {
	err := ioutil.WriteFile(gyazoFileName, []byte(newgyazoid), 0644)
	if err != nil {
		Error.Println("Error writing to ", gyazoFileName, err)
	}
}

func makeNewScreenshotUploadItAndCopyURLToClipboard(quality int, url string) {
	Info.Println("Upload URL:", url, " ; Quality:", quality)
	fname := captureScreenshot(quality)
	setStatus("Picture stored to: " + fname)
	metadata := screenShotMetadata{
		App:   "the app",
		Title: "the title",
		URL:   "the url",
		Note:  "the note",
	}
	Info.Println("Uploading ", fname)
	remoteURL, remoteError := postFile(fname, metadata, url)
	if remoteError != nil {
		Error.Println("Error uploading to remote server URL", url, remoteError.Error())
	} else {
		Info.Println("Server returned: ", remoteURL)
		setClipboard(remoteURL)
		setStatus("Picture uploaded to: " + remoteURL)
		os.Remove(fname)
	}
}

func setClipboard(txt string) {
	clipboard := gtk.NewClipboardGetForDisplay(gdk.DisplayGetDefault(), gdk.SELECTION_PRIMARY)
	clipboard.SetText(txt)
	gtk.MainIterationDo(true)
	clipboard.Store()
	gtk.MainIterationDo(true)
}

func mainGKTWindow() {
	gtk.Init(&os.Args)
	window := gtk.NewWindow(gtk.WINDOW_TOPLEVEL)
	window.SetPosition(gtk.WIN_POS_CENTER)
	window.SetTitle("Snima4")
	window.SetIconName("gtk-dialog-info")
	window.SetSizeRequest(480, 220)
	window.Connect("destroy", func(ctx *glib.CallbackContext) {
		Trace.Println("on destroy", ctx.Data().(string))
		gtk.MainQuit()
	}, "die")

	vbox := gtk.NewVBox(false, 0)
	window.Add(vbox)

	settingsFrame := gtk.NewFrame("Settings:")
	vbox.PackStart(settingsFrame, false, false, 2)

	frameboxSettings := gtk.NewVBox(false, 2)
	settingsFrame.Add(frameboxSettings)

	labelUpload := gtk.NewLabel("Upload URL after the screenshot is taken:")
	labelUpload.SetAlignment(0, 0)
	frameboxSettings.Add(labelUpload)
	uploadURLInput := gtk.NewEntry()
	uploadURLInput.SetText(defaultURL)
	frameboxSettings.PackStart(uploadURLInput, true, false, 0)

	labelQuality := gtk.NewLabel("Compression quality:")
	labelQuality.SetAlignment(0, 1)

	frameboxSettings.Add(labelQuality)
	qualityNumberInput := gtk.NewEntry()
	qualityNumberInput.SetText(strconv.Itoa(defaultQuality))
	frameboxSettings.PackStart(qualityNumberInput, true, false, 0)

	shotButton := gtk.NewButtonWithLabel("Shoot!")
	shotButton.Clicked(func() {
		Trace.Println("shotButton clicked")
		quality, errQuality := strconv.Atoi(qualityNumberInput.GetText())
		if errQuality != nil {
			Error.Println("Error while getting the quality. Reverting to the default.")
			quality = defaultQuality
		}
		quality = maxInt(10, minInt(95, quality))
		url := uploadURLInput.GetText()
		makeNewScreenshotUploadItAndCopyURLToClipboard(quality, url)
	})
	shotButton.SetSizeRequest(180, 60)
	vbox.PackStart(shotButton, true, true, 2)

	statusbar = gtk.NewStatusbar()
	statusbarContextID = statusbar.GetContextId("status")
	statusbar.Push(statusbarContextID, "OK")
	vbox.PackEnd(statusbar, false, false, 0)

	shotButton.GrabFocus()
	window.ShowAll()
	gtk.Main()
}

func initGyazoID() {
	gyazoFileLocation, err := homedir.Expand(gyazoFileName)
	if err != nil {
		return
	}
	x, err := ioutil.ReadFile(gyazoFileLocation)
	if err != nil {
		return
	}
	gyazoid = string(x)
}

func main() {
	initLoggers(ioutil.Discard, os.Stdout, os.Stdout, os.Stderr)
	Info.Println("Start")
	initGyazoID()
	mainGKTWindow()
	Info.Println("End")
}
