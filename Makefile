PKGVERSION := $(shell cat nfpm.yaml | shyaml get-value version)
ICONS := Icon/16x16/snima4.png Icon/128x128/snima4.png Icon/32x32/snima4.png Icon/48x48/snima4.png Icon/256x256/snima4.png Icon/snima4.pixmap.png

all:
	go build

clean:
	rm -rf snima4 *.deb *.rpm $(ICONS)

snima4:
	go build
	strip snima4
	upx snima4

deb: icons snima4 nfpm.yaml
	nfpm pkg -t snima4_$(PKGVERSION)_amd64.deb

rpm: icons snima4 nfpm.yaml
	nfpm pkg -t snima4_$(PKGVERSION)_amd64.rpm

packages: deb rpm

icons: $(ICONS)

Icon/16x16/snima4.png: snima4.png
	convert snima4.png -resize '16x16'   Icon/16x16/snima4.png
Icon/128x128/snima4.png: snima4.png
	convert snima4.png -resize '128x128' Icon/128x128/snima4.png
Icon/32x32/snima4.png: snima4.png
	convert snima4.png -resize '32x32'   Icon/32x32/snima4.png
Icon/48x48/snima4.png: snima4.png
	convert snima4.png -resize '48x48'   Icon/48x48/snima4.png
Icon/256x256/snima4.png: snima4.png
	convert snima4.png -resize '256x256' Icon/256x256/snima4.png
Icon/snima4.pixmap.png: snima4.png
	convert snima4.png -resize '48x48'   Icon/snima4.pixmap.png
